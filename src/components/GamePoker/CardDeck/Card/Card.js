import React, {Component} from 'react';

const cardSuits = {
   h: ['hearts', '♥'],
   s: ['spades', '♠'],
   d: ['diams', '♦'],
   c: ['clubs', '♣'],
};

class Card extends Component {
   constructor(props) {
      super(props);
      this.stateSwitcher = () => {
         if (this.switchCard) {
            this.switchCard = false;
            return {
               cardHtml: <div className="card back" onClick={this.flipCard}>*</div>,
            };
         } else {
            this.switchCard = true;
            return {
               cardHtml: (
                 <div
                   className={"card rank-" + this.props.rank.toLowerCase() + " " + cardSuits[this.props.suit.toLowerCase()][0]} onClick={this.flipCard}>
                    <span className="rank">{this.props.rank}</span>
                    <span className="suit">{cardSuits[this.props.suit.toLowerCase()][1]}</span>
                 </div>),
            };
         }
      }
      this.state = this.stateSwitcher();
   }

   flipCard = () => {
      if (!this.props.flipChanger()){
         this.setState(this.stateSwitcher());
         this.props.switcher(this.props.cardIndex);
      }
   };

   render() {
      return this.state.cardHtml;
   }
}

export default Card;