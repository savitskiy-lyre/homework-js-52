import React, {Component} from 'react';

class PokerHand extends Component {

   takeCards = () => {
      return this.props.state.fiveCards.map((card) => {
         return {rank: card.props.rank, suit: card.props.suit}
      });

   };

   getOutcome = () => {
      const cards = this.takeCards();
      const cardsInfo = {};
      for (const card of cards) {
         if (!cardsInfo[card.rank]) {
            cardsInfo[card.rank] = [card];
         } else {
            cardsInfo[card.rank].push(card);
         }
      }
      let magicSuit = '';
      Object.keys(cardsInfo).forEach((key) => {
         magicSuit += cardsInfo[key][0]['suit'];
      });
      let magicRank = '';
      Object.keys(cardsInfo).forEach((key) => {
         if (cardsInfo[key][0]['rank'] !== '10') {
            magicRank += cardsInfo[key][0]['rank'];
         } else {
            magicRank += 1;
         }
      });

      let message = 'None';

      const isHardCombo = () => {

         if (magicRank.length === 2) {
            if (cardsInfo[magicRank[0]].length === 1 || cardsInfo[magicRank[0]].length === 4) {
               message = 'Four of a kind';
               return true;
            } else {
               message = 'Full house';
               return true;
            }
         }

         if (magicRank.length === 5) {
            const examplesObj = {
               1: 10,
               2: 2,
               3: 3,
               4: 4,
               5: 5,
               6: 6,
               7: 7,
               8: 8,
               9: 9,
               'J': 11,
               'Q': 12,
               'K': 13,
               'A': 14,
            }
            const magicNumber = magicRank.split('').map((letter) => {
               return examplesObj[letter]
            });
            const magicTotal = magicNumber.reduce((total = 0, number) => {
               return total + number;
            });
            const sortedNumbers = magicNumber.sort((a, b) => {
               return a - b;
            })

            let neededTotal = 0;
            for (let i = 0; i < 5; i++) {
               neededTotal += sortedNumbers[0] + i;
            }
            let neededTotalAce = 0;
            for (let i = 0; i < 4; i++) {
               neededTotalAce += sortedNumbers[0] + i;
            }

            const isSameSuit = magicSuit.split(magicSuit[0]);
            if (isSameSuit.length === 6){
               if (magicTotal === 60){
                  message = 'Royal flush';
                  return true;
               }
               if (magicTotal === neededTotal){
                  message = 'Straight flush';
                  return true;
               }
            }

            if ((magicTotal - 14 === neededTotalAce && sortedNumbers[sortedNumbers.length - 1] === 14) || magicTotal === neededTotal){
               message = 'Straight';
               return true;
            }
         }
      };

      const isFlush = () => {
         if (magicSuit.length === 5) {
            let count = 0;
            for (const letter of magicSuit) {
               if (magicSuit[0] === letter) {
                  count++;
               }
            }
            if (count === 5) {
               return true;
            }
         }
      };
      if (isHardCombo()) {

      } else {
         for (const key in cardsInfo) {
            if (cardsInfo.hasOwnProperty(key)) {

               if (isFlush()) {
                  message = 'Flush';
                  break;
               }

               if (cardsInfo[key].length > 2) {
                  message = 'Three of a kind';
                  break;
               }

               if (cardsInfo[key].length > 1) {
                  if (message === 'One pair') {
                     message = 'Two pairs';
                     continue;
                  }
                  message = 'One pair';
               }
            }
         }
      }
      return message;
   };

   render() {
      return (
        <div>
           <p>
              <b>Combo: </b>{this.getOutcome()}
           </p>
        </div>
      );
   };
}

export default PokerHand;