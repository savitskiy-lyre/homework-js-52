import React, {Component} from 'react';
import CardDeck from "./CardDeck/CardDeck";

class GamePoker extends Component {

   render() {
      return (
        <div className="playingCards">
           <CardDeck/>
        </div>
      );
   }
}

export default GamePoker;